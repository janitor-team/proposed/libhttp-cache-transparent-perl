Source: libhttp-cache-transparent-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Nick Morrott <knowledgejunkie@gmail.com>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libtest-pod-coverage-perl,
                     libtest-pod-perl,
                     libtest-requiresinternet-perl,
                     libwww-perl,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libhttp-cache-transparent-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libhttp-cache-transparent-perl.git
Homepage: https://metacpan.org/release/HTTP-Cache-Transparent

Package: libhttp-cache-transparent-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libwww-perl
Description: Perl module used to transparently cache HTTP requests
 HTTP::Cache::Transparent is an implementation of HTTP GET that keeps a
 local cache of fetched pages to avoid fetching the same data from the
 server if it hasn't been updated. The cache is stored on disk and is
 thus persistent between invocations.
 .
 The HTTP headers If-Modified-Since and ETag are used to let the server
 decide if the version in the cache is up-to-date or not.  All
 HTTP requests are made through the LWP module. Data is stored on disk
 by the Storable module. Digest::MD5 is used for creating a hash of the
 URL.
